package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"reflect"
	"strings"
	"test_db"
)

func errPrint(err *error) {
	if &err != nil {
		fmt.Println(&err)
	}
}
func writeJSONtoFile(obj any, name string) {
	f, _ := os.Create(fmt.Sprintf("%s.json", name))
	defer f.Close()
	as_json, _ := json.MarshalIndent(obj, "", "\t")
	f.Write(as_json)
}
func openLogFile(path string) (*os.File, error) {
	logFile, err := os.OpenFile(path, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		return nil, err
	}
	return logFile, nil
}
func generateFormData(arango test_db.ArgonAnswer) test_db.FormDataForMysql {
	st := make([]string, 0)
	structTeg := test_db.SuperTag{
		Tag: test_db.Tag{
			Id:           arango.DATA.Rows[0].Author.UserId,
			Name:         "Клиент",
			Key:          "client",
			ValuesSource: 0,
		},
		Value: arango.DATA.Rows[0].Author.UserName,
	}
	jsonStruct, _ := json.MarshalIndent(structTeg, "", "")
	myString := string(jsonStruct[:])
	myString = strings.ReplaceAll(myString, "\n", "")
	myString = strings.ReplaceAll(myString, "\t", "")
	myString = strings.ReplaceAll(myString, " ", "")
	st = append(st, myString)
	resoult := test_db.FormDataForMysql{
		PeriodStart:         arango.DATA.Rows[0].Params.Period.Start,
		PeriodEnd:           arango.DATA.Rows[0].Params.Period.End,
		PeriodKey:           arango.DATA.Rows[0].Params.Period.TypeKey,
		IndicatorToMoId:     arango.DATA.Rows[0].Params.IndicatorToMoId,
		IndicatorToMoFactId: 0,
		Value:               1,
		FactTime:            arango.DATA.Rows[0].Time.Format("2006-01-02"),
		IsPlan:              0,
		SuperTag:            st,
		AuthUserId:          40,
		Comment:             arango.DATA.Rows[0].Params.Platform,
	}
	return resoult
}
func generateUrlValuesWithReflect(formDataJson test_db.FormDataForMysql) (string, io.Reader, error) {
	data := url.Values{}
	data2 := map[string]string{}

	buf := new(bytes.Buffer)
	bw := multipart.NewWriter(buf)

	val := reflect.ValueOf(formDataJson)
	typeOfValue := val.Type()
	numFields := typeOfValue.NumField()
	//if numFields == 0 {
	//	return map[string]string{}
	//}

	for i := 0; i < numFields; i++ {
		jsonTag := typeOfValue.Field(i).Tag
		fieldValue := val.Field(i)
		//fmt.Println(jsonTag.Get("json"), fmt.Sprintf("%v", fieldValue))
		data.Add(jsonTag.Get("json"), fmt.Sprintf("%v", fieldValue))
		data2[jsonTag.Get("json")] = fmt.Sprintf("%v", fieldValue)

		p1w, _ := bw.CreateFormField(string(jsonTag.Get("json")))
		p1w.Write([]byte(fmt.Sprintf("%v", fieldValue)))
		//fw, err := writer.CreateFormField(jsonTag.Get("json"))
		//if err != nil {
		//}
		//_, err = io.Copy(fw, strings.NewReader(fmt.Sprintf("%v", fieldValue)))
		////if err != nil {
		//	return err
		//}
	}
	//fmt.Println(data)
	bw.Close()
	//typeContent:=writer.FormDataContentType()
	return bw.FormDataContentType(), buf, nil
}
func getAuthHeader(token string) string {
	return "Bearer " + token
}
func main() {
	fileLog, err := openLogFile("./mylogs.log")
	if err != nil {
		errorLog := log.New(fileLog, "[error]", log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
		errorLog.Println("this is error")
		log.Fatal(err)
	}
	errorLog := log.New(fileLog, "[START LOG]", log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
	errorLog.Println("log file start")

	if err := godotenv.Load(); err != nil {
		errorLog := log.New(fileLog, "[error]", log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
		errorLog.Println("this is error")
		log.Fatalf("you have error with dotenv file %s", err.Error())
	}

	client := http.Client{}

	postData := strings.NewReader(fmt.Sprintf("login=%s&password=%s",
		os.Getenv("ARANGO_DB_LOGIN"), os.Getenv("ARANGO_DB_PASSWORD")))
	//fmt.Println(postData)
	req, err := http.NewRequest("GET", os.Getenv("AUTH_URL"), postData)
	errPrint(&err)
	resp, err := client.Do(req)
	errPrint(&err)
	var cookie []*http.Cookie
	cookie = resp.Cookies()
	writeJSONtoFile(cookie, "cookies")
	//fmt.Println(cookie)
	infoLog := log.New(fileLog, "[INFO]", log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
	infoLog.Println("write cooke")

	jsonBody, err := ioutil.ReadFile("request_param.json")
	if err != nil {
		log.Fatal(err)
	}
	bodyReader := bytes.NewReader(jsonBody)
	//fmt.Println(jsonBody)
	req2, _ := http.NewRequest("GET", os.Getenv("ARANGO_URL"), bodyReader)
	req2.Header.Set("Content-Type", "application/json")
	for i := range cookie {
		req2.AddCookie(cookie[i])
	}
	resp2, err := client.Do(req2) //send request
	if err != nil {
		return
	}
	argonAnswer := test_db.ArgonAnswer{}
	json.NewDecoder(resp2.Body).Decode(&argonAnswer)
	writeJSONtoFile(argonAnswer, "answer_from_arango")
	//fmt.Println(argonAnswer)
	infoLog = log.New(fileLog, "[INFO]", log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
	infoLog.Println("write Arango answer to file")
	newStruct := generateFormData(argonAnswer)
	writeJSONtoFile(newStruct, "request_to_mysql")
	infoLog = log.New(fileLog, "[INFO]", log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
	infoLog.Println("write request_to_mysql to file")
	contType, reader, err := generateUrlValuesWithReflect(newStruct)

	req3, _ := http.NewRequest("POST", os.Getenv("MYSQL_URL"), reader)
	req3.Header.Set("Content-Type", contType)
	//req3.Header.Set("Content-Type", writer.FormDataContentType())
	auth := getAuthHeader(os.Getenv("TOKEN"))
	req3.Header.Add("Authorization", auth)
	for i := range cookie {
		req3.AddCookie(cookie[i])
	}
	//req4.Header.Add("Authorization", auth)
	//fmt.Println(req3.Form)
	resp3, err := client.Do(req3)
	if err != nil {
		return
	}

	body, _ := ioutil.ReadAll(resp3.Body)
	fmt.Println(string(body))
	writeJSONtoFile(body, "resoult")
	//defer req3.Body.Close()
	//io.Copy(os.Stdout, req3.Body)
	//req3, _ := http.NewRequest("POST", os.Getenv("MYSQL_URL"), data)
	//req3.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	//aa := url.Values{}
	//fmt.Println(reflect.TypeOf(aa))
	resp3.Body.Close()

}
