package test_db

import "time"

type ArgonAnswer struct {
	MESSAGES struct {
		Error   interface{} `json:"error"`
		Warning interface{} `json:"warning"`
		Info    interface{} `json:"info"`
	} `json:"MESSAGES"`
	DATA struct {
		Page       int `json:"page"`
		PagesCount int `json:"pages_count"`
		RowsCount  int `json:"rows_count"`
		Rows       []struct {
			Id     string `json:"_id"`
			Key    string `json:"_key"`
			Rev    string `json:"_rev"`
			Author struct {
				MoId     int    `json:"mo_id"`
				UserId   int    `json:"user_id"`
				UserName string `json:"user_name"`
			} `json:"author"`
			Group  string `json:"group"`
			Msg    string `json:"msg"`
			Params struct {
				IndicatorToMoId int `json:"indicator_to_mo_id"`
				Period          struct {
					End     string `json:"end"`
					Start   string `json:"start"`
					TypeId  int    `json:"type_id"`
					TypeKey string `json:"type_key"`
				} `json:"period"`
				Platform string `json:"platform"`
			} `json:"params"`
			Time time.Time `json:"time"`
			Type string    `json:"type"`
		} `json:"rows"`
	} `json:"DATA"`
	STATUS string `json:"STATUS"`
}
