package test_db

type Tag struct {
	Id           int    `json:"id"`
	Name         string `json:"name"`
	Key          string `json:"key"`
	ValuesSource int    `json:"values_source"`
}
type SuperTag struct {
	Tag   Tag    `json:"tag"`
	Value string `json:"value"`
}
type FormDataForMysql struct {
	PeriodStart         string   `json:"period_start"`
	PeriodEnd           string   `json:"period_end"`
	PeriodKey           string   `json:"period_key"`
	IndicatorToMoId     int      `json:"indicator_to_mo_id"`
	IndicatorToMoFactId int      `json:"indicator_to_mo_fact_id"`
	Value               int      `json:"value"`
	FactTime            string   `json:"fact_time"`
	IsPlan              int      `json:"is_plan"`
	SuperTag            []string `json:"supertags"`
	AuthUserId          int      `json:"auth_user_id"`
	Comment             string   `json:"comment"`
}
type Tags struct {
	Tag struct {
		Id           int    `json:"id"`
		Name         string `json:"name"`
		Key          string `json:"key"`
		ValuesSource int    `json:"values_source"`
	} `json:"tag"`
	Value string `json:"value"`
}
