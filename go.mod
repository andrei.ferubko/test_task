module test_db

go 1.21

require (
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	golang.org/x/sys v0.13.0 // indirect
)
